<!DOCTYPE html>
<?php $root = $_SERVER['DOCUMENT_ROOT']; ?>
<html lang=es>
<?php include $root . '/head.html'; ?>
<body>
    <?php include $root . '/header.html'; ?>
    <p>Página personal de Suguivy, en la que se hablará sobre ciencias de la computación, software libre y GNU/Linux, entre otras cosas, y en la que se promueve la libre información.</p>
    <?php include $root . '/footer.html'; ?>
</body>
</html>
