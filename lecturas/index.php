<!DOCTYPE html>
<?php $root = $_SERVER['DOCUMENT_ROOT']; ?>
<html lang=es>
<?php include $root . '/head.html'; ?>
<body>
<?php include $root . '/header.html'; ?>
<div class='entry'>
<h3><a href='https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book.html'>Structure and Interpretation of Computer Programs</a></li>
</h3>
<p>Este fascinante libro se adentra en los fundamentos de las ciencias de la computación, utilizando el lenguaje de programación Scheme. Aborda diferentes temas como la iteración y la recursividad o la construcción de diferentes tipos y estructuras de datos, y en los siguientes capítulos se mete aún más a fodo, hablando sobre compiladores e intérpretes. Personalmente es un libro que recomiendo a la gente que sepa un mínimo de programación, ya que para alguien que esté empezando puede ser un poco difícil.</p>
</div>
<?php include $root . '/footer.html'; ?>
</body>
</html>
