<!DOCTYPE html>
<?php $root = $_SERVER['DOCUMENT_ROOT']; ?>
<html lang=es>
<?php include $root . '/head.html'; ?>
<body>
<?php
include $root . '/header.html';

class MyDB extends SQLite3 {
    function __construct() {   
        $this->open('entries.db');
    }   
}

$db = new MyDB();

if (!$db) {
    die('Database cannot be read');
}

$sql = 'SELECT * FROM entries';

$ret = $db->query($sql);

while ($row = $ret->fetchArray(SQLITE3_ASSOC)) {
    echo "<div class=\"entry\">";
    echo "<h2><a href=\"article.php?id=" . $row['ID'] . "\">" . $row['TITLE'] . "</a></h2>\n";
    echo substr($row['CONTENT'], 0, 256) . '...';
    echo "</div><br/>";
}

include $root . '/footer.html';
?>
</body>
</html>
