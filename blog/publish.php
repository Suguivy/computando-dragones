<!DOCTYPE html>
<?php $root = $_SERVER['DOCUMENT_ROOT']; ?>
<html lang=es>
<body>
<?php
include $root . '/lib/parsedown/Parsedown.php';
$parser = new Parsedown();
$title = $_POST['title'];
$content = SQLite3::escapeString($parser->text($_POST['content']));
$date = time();
$password = $_POST['password'];
$hash = "$2y$10$3MaU8DguGtwdAEEP.8zClOdjLRNceOA0.ht662QUoRGbgHIStmcRu";

class MyDB extends SQLite3 {
    function __construct() {   
        $this->open('entries.db');
    }   
}
if (password_verify($password, $hash)) {
    $db = new MyDB();
    $sql = "INSERT INTO entries (TITLE, CONTENT, DATE) VALUES ('" . $title . "','" . $content . "'," . $date . ")";
    $ret = $db->exec($sql);
    if (!$ret) {
        echo $db->lastErrorMsg();
    } else {
        echo 'Artículo publicado correctamente';
    }
} else {
    echo 'Contraseña incorrecta';
}

$db->close();
?>
</body>
</html>
