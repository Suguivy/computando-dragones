<!DOCTYPE html>
<?php $root = $_SERVER['DOCUMENT_ROOT']; ?>
<html lang=es>
<?php include $root . '/head.html'; ?>
<body>
<?php
include $root . '/header.html';
include $root . '/lib/parsedown/Parsedown.php';

$preview = !isset($_GET['id']);

if ($preview) { // Si es para una previsualización de la edición
    $parser = new Parsedown();
    $title = $_POST['title'];
    $content = $parser->text($_POST['content']);
    $date = date('j F, o');
} else {
    class MyDB extends SQLite3 {
        function __construct() {   
            $this->open('entries.db');
        }   
    }

    $db = new MyDB();
    $sql = 'SELECT * FROM entries WHERE ID=' . $_GET['id'];
    $ret = $db->query($sql);
    $row = $ret->fetchArray(SQLITE3_ASSOC);

    $title = $row['TITLE'];
    $content = $row['CONTENT'];
    $date = date('j F, o', $row['DATE']);
}

echo "<h1>" . $title . "</h1>\n";
echo $content . "<br/>\n";
echo $date;

include $root . '/footer.html';
?>
</body>
</html>
